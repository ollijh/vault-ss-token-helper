{
  description = "A Vault token helper that stores the tokens in Secret Service.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nci.url = "github:yusdacra/nix-cargo-integration";
    nci.inputs.nixpkgs.follows = "nixpkgs";

    treefmt.url = "github:numtide/treefmt-nix";
    treefmt.inputs.nixpkgs.follows = "nixpkgs";

    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";

    parts.url = "github:hercules-ci/flake-parts";
    parts.inputs.nixpkgs-lib.follows = "nixpkgs";
  };

  outputs =
    inputs@{
      self,
      nixpkgs,
      nci,
      treefmt,
      parts,
      devshell,
    }:
    let
      crateName = "vault-ss-token-helper";
    in
    parts.lib.mkFlake { inherit inputs; } (
      { moduleWithSystem, ... }:
      {
        systems = [
          "x86_64-linux"
          "aarch64-linux"
        ];

        imports = [
          nci.flakeModule
          parts.flakeModules.easyOverlay
          treefmt.flakeModule
          devshell.flakeModule
        ];

        perSystem =
          {
            self',
            system,
            pkgs,
            config,
            lib,
            ...
          }:
          let
            crateOutputs = config.nci.outputs.${crateName};
          in
          {
            _module.args.pkgs = import nixpkgs {
              inherit system;
              config.allowUnfree = true;
            };

            nci.projects.${crateName} = {
              path = ./.;
              drvConfig.mkDerivation.meta = {
                description = "A Vault token helper that stores the tokens in Secret Service.";
                mainProgram = crateName;
                homepage = "https://codeberg.org/ollijh/vault-ss-token-helper";
              };
            };

            nci.crates.${crateName}.export = true;

            treefmt = {
              projectRootFile = "flake.nix";
              programs.nixfmt-rfc-style.enable = true;
              programs.rustfmt.enable = true;
            };

            overlayAttrs.${crateName} = crateOutputs.packages.release;

            devshells.default.devshell = {
              packages =
                [ config.treefmt.build.wrapper ]
                ++ (with pkgs; [
                  rust-analyzer
                  cargo-edit
                ]);
              packagesFrom = [ crateOutputs.devShell ];
            };

            packages.default = crateOutputs.packages.release;

            checks.default =
              let
                inherit (lib.nixos) runTest;
                rootToken = "phony-secret";
              in
              runTest {
                name = "default";
                hostPkgs = pkgs;

                nodes.machine =
                  { config, ... }:
                  {
                    imports = [ self.nixosModules.default ];

                    nixpkgs.config.allowUnfree = true;

                    users.users.alice = {
                      isNormalUser = true;
                      password = "foobar";
                    };

                    programs.bash.promptInit = "PS1='#\\#: '";

                    services = {
                      gnome.gnome-keyring.enable = true;

                      vault = {
                        enable = true;
                        dev = true;
                        devRootTokenID = rootToken;
                        ss-token-helper.enable = true;
                      };
                    };

                    environment = {
                      systemPackages = with pkgs; [
                        vault
                        libsecret
                      ];
                      variables = {
                        VAULT_ADDR = "http://${config.services.vault.address}";
                      };
                    };
                  };

                testScript = ''
                  machine.wait_for_open_port(8200)
                  machine.wait_until_tty_matches("1", "login: ")
                  machine.send_chars("alice\n")
                  machine.wait_until_tty_matches("1", "login: alice")
                  machine.wait_until_succeeds("pgrep login")
                  machine.wait_until_tty_matches("1", "Password: ")
                  machine.send_chars("foobar\n")
                  machine.wait_until_tty_matches("1", "#1: ")

                  machine.send_chars("vault login ${rootToken}\n")
                  machine.wait_until_tty_matches("1", "You are now authenticated")
                  machine.wait_until_tty_matches("1", "#2: ")

                  machine.send_chars("secret-tool lookup vault_addr $VAULT_ADDR > /tmp/vault-token\n")
                  machine.wait_until_tty_matches("1", "#3: ")

                  machine.succeed("test `cat /tmp/vault-token` = '${rootToken}'")
                '';
              };
          };

        flake.nixosModules.default = moduleWithSystem (
          perSystem@{ config }:
          {
            config,
            lib,
            pkgs,
            ...
          }:
          let
            inherit (lib)
              getExe
              mkEnableOption
              mkPackageOption
              mkIf
              ;
            inherit (pkgs) writeText;
            cfg = config.services.vault.ss-token-helper;
          in
          {
            options.services.vault.ss-token-helper = {
              enable = mkEnableOption "SecretService token helper";
              package = mkPackageOption pkgs crateName { };
            };

            config = mkIf cfg.enable {
              nixpkgs.overlays = [ self.overlays.default ];

              environment.sessionVariables.VAULT_CONFIG_PATH = writeText "vault.hcl" ''
                token_helper = "${getExe cfg.package}"
              '';
            };
          }
        );
      }
    );
}
