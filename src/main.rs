use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;
use secret_service::EncryptionType;
use secret_service::SecretService;
use std::collections::HashMap;
use std::env;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
enum Command {
    Get,
    Store,
    Erase,
}

#[tokio::main]
async fn main() -> Result<()> {
    let cmd = Command::parse();

    let service = SecretService::connect(EncryptionType::Dh).await?;

    let collection = service.get_default_collection().await?;
    collection.ensure_unlocked().await?;

    let vault_addr = env::var("VAULT_ADDR")?;
    let maybe_vault_namespace = env::var("VAULT_NAMESPACE").ok();

    let mut terms: HashMap<&str, &str> = HashMap::new();
    terms.insert("vault_addr", &vault_addr);
    if let Some(ref vault_namespace) = maybe_vault_namespace {
        terms.insert("vault_namespace", vault_namespace);
    }

    let mut stdout = tokio::io::stdout();
    let mut stdin = tokio::io::stdin();

    match cmd {
        Command::Get => match collection.search_items(terms).await?.as_mut_slice() {
            [item] => {
                let secret = item.get_secret().await?;
                stdout.write_all(&secret).await?;
                Ok(())
            }
            [] => Ok(()),
            _ => Err(anyhow!("Multiple items match")),
        },
        Command::Store => {
            let label = if let Some(ref vault_namespace) = maybe_vault_namespace {
                format!("Vault token for {} in {}", vault_addr, vault_namespace)
            } else {
                format!("Vault token for {}", vault_addr)
            };
            let mut secret: Vec<u8> = Vec::new();
            stdin.read_to_end(&mut secret).await?;
            let secret = secret.strip_suffix(b"\n").unwrap_or(&secret);
            collection
                .create_item(&label, terms, secret, true, "text/plain")
                .await?;
            Ok(())
        }
        Command::Erase => match collection.search_items(terms).await?.as_mut_slice() {
            [item] => {
                item.delete().await?;
                Ok(())
            }
            [] => Ok(()),
            _ => Err(anyhow!("Multiple items match")),
        },
    }
}
